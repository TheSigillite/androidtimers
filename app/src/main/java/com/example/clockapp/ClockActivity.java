package com.example.clockapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class ClockActivity extends AppCompatActivity {

    Date nowtim;
    TextView current;
    TextView york;
    TextView weaboo;
    TextView internat;


    Thread thread = new Thread() {

        @Override
        public void run() {
            try {
                while (!thread.isInterrupted()) {
                    Thread.sleep(1000);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            current.setText(parseTimeZones("Europe/Warsaw"));
                            york.setText(parseTimeZones("America/New_York"));
                            weaboo.setText(parseTimeZones("Asia/Tokyo"));
                            internat.setText(parseTimeZones("Greenwich"));
                        }
                    });
                }
            } catch (InterruptedException e) {
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clock);
        current = findViewById(R.id.Local);
        york = findViewById(R.id.NYT);
        weaboo = findViewById(R.id.WeebTime);
        internat = findViewById(R.id.Greenwich);
        Intent intent = getIntent();
        String ntime = intent.getStringExtra("nowtime");
        nowtim = (Date) intent.getSerializableExtra("nowtim");
        current.setText(ntime);
        thread.start();
    }


    private String parseTimeZones(String currenttime){
        DateFormat thiszone = new SimpleDateFormat("HH:mm:ss");
        thiszone.setTimeZone(TimeZone.getTimeZone(currenttime));
        nowtim = Calendar.getInstance().getTime();
        String a = thiszone.format(nowtim);
        return a;
    }
}

package com.example.clockapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void TellTime(View view) {
        Button tbutton = findViewById(R.id.StartNewActButton);
        Date cal = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        String nowtime = dateFormat.format(cal);
        Intent intent = new Intent(this,ClockActivity.class);
        intent.putExtra("tim",cal);
        intent.putExtra("nowtime",nowtime);
        startActivity(intent);

    }
}
